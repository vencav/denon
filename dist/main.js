/* Remove Shoptet stylesheet - this part should be inserted
* directly in "Header" section of /admin/html-kody/
var styles = document.querySelectorAll('link[rel="stylesheet"]');
for (i = 0; i < styles.length; i++) {
    if (styles[i].getAttribute('href').indexOf('main') !== -1) {
        styles[i].parentNode.removeChild(styles[i]);
    }
}
 */

var i;
const i18n = {
    'ADD_TO_CART_BUTTON_TEXT': 'Koupit',
    'DETAIL_BUTTON_TEXT': 'Detail',
    'ALTERNATIVE_PRODUCTS_HEADING_TEXT': 'Alternativní produkty',
    'CHOOSE_A_PICKUP': 'Vyberte odběrné místo'
};

function detectScroll(direction) {
    var $html = $("html");
    var classToRemove = 'up' === direction ? 'scrolled-down' : 'scrolled-up';
    if ($(window).scrollTop() > 0) {
        $html.addClass("scrolled scrolled-" + direction);
        $html.removeClass(classToRemove);
    } else {
        $("html").removeClass("scrolled scrolled-up scrolled-down")
    }
}

function generateLinks() {
    var navigationToggle = document.createElement('a');
    navigationToggle.setAttribute('data-target', 'navigation');
    navigationToggle.setAttribute('href', '#');
    navigationToggle.classList.add('toggle-window');

    var searchToggle = document.createElement('a');
    searchToggle.setAttribute('data-target', 'search');
    searchToggle.setAttribute('href', '#');
    searchToggle.classList.add('toggle-window');

    var loginToggle = document.createElement('a');
    loginToggle.setAttribute('data-target', 'login');
    loginToggle.setAttribute('href', '#');
    loginToggle.classList.add('toggle-window');

    var linksWrapper = document.querySelector('.navigation-buttons');
    linksWrapper.appendChild(loginToggle);
    linksWrapper.appendChild(searchToggle);
    linksWrapper.appendChild(navigationToggle);
}

function handleWithMenu() {
    var i, item, link, arrow;
    var thirdLevelParentItems = document.querySelectorAll('.has-third-level');
    for (i = 0; i < thirdLevelParentItems.length; i++) {
        item = thirdLevelParentItems[i];
        link = item.querySelector('div > a');

        var image = item.querySelector('.menu-image');
        var div = item.querySelector('div');
        div.appendChild(image);

        arrow = document.createElement('span');
        arrow.classList.add('submenu-arrow');
        link.appendChild(arrow);
        (function(item) {
            arrow.addEventListener('click', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                item.classList.toggle('exp');
            });
        })(item);
    }

    var thirdLevelItems = document.querySelectorAll('.menu-level-3 > li');
    for (i = 0; i < thirdLevelItems.length; i++) {
        item = thirdLevelItems[i];
        link = item.querySelector('a');
        item.innerHTML = '';
        item.appendChild(link);
    }

    shoptet.scripts.setCustomCallback('shoptet.menu.showSubmenu', function() {
        unveilImages();
    });
}

function adjustProductsList(products) {
    for (var i = 0; i < products.length; i++) {
        var product = products[i];
        var link = product.querySelector('a.name');
        if (link && !product.classList.contains('js-processed')) {
            product.classList.add('js-processed');
            // Create detail buttons
            var detailLinkWrapper = product.querySelector('.p-tools');
            if (detailLinkWrapper) {
                var href = link.getAttribute('href');
                var detailLink = document.createElement('a');
                detailLink.setAttribute('href', href);
                detailLink.classList.add('btn', 'btn-outline-primary');
                detailLink.innerHTML = i18n['DETAIL_BUTTON_TEXT'];
                detailLinkWrapper.appendChild(detailLink);
            }

            // Move product name above the image
            var image = product.querySelector('.image');
            if (image) {
                image.parentNode.insertBefore(link, image);
            }

            // Move availability
            var availabilityWrapper = product.querySelector('.p-in-in');
            var availabilityNewLocation = product.querySelector('.prices');
            if (availabilityWrapper && availabilityNewLocation) {
                availabilityNewLocation.parentNode.insertBefore(availabilityWrapper, availabilityNewLocation);
            }
        }
    }
}

function renameButtons() {
    var buttons = document.querySelectorAll('.add-to-cart-button');
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].innerHTML = i18n['ADD_TO_CART_BUTTON_TEXT'];
    }
}

function cloneSearch() {
    var search = $('.search');
    var cloned = search.clone();
    var userAction = document.querySelectorAll('.user-action-in')[0];
    var div = document.createElement('div');
    div.classList.add('user-action-search', 'popup-widget', 'search-widget');
    var innerDiv = document.createElement('div');
    innerDiv.classList.add('popup-widget-inner');
    innerDiv.appendChild(cloned[0]);
    div.appendChild(innerDiv);
    userAction.appendChild(div);
}

function adjustHeader() {
    var sitenameWrapper = document.querySelectorAll('.header-top > div:first-child');
    if (sitenameWrapper) {
        sitenameWrapper[0].classList.add('site-name-wrapper');
    }
}

function adjustHeaderButtons() {
    var buttons = document.querySelectorAll('.header-top .search .btn');
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].innerHTML = '';
    }
}

function moveFlags() {
    var body = document.getElementsByTagName('body')[0];
    if (!body.classList.contains('type-detail')) {
        return false;
    }
    var flags = document.querySelectorAll('.flags-default');
    var flagsWrapper = document.querySelectorAll('.p-image');
    if (flags[0] && flagsWrapper[0]) {
        flagsWrapper[0].appendChild(flags[0]);
    }
}

function moveRelated() {
    var body = document.getElementsByTagName('body')[0];
    if (!body.classList.contains('type-detail')) {
        return false;
    }
    var alternative = document.getElementById('productsAlternative');
    if (!alternative) {
        return false;
    }
    var wrapper = document.querySelectorAll('.p-detail-tabs-wrapper');
    if (!wrapper[0]) {
        return false;
    }
    wrapper[0].parentNode.insertBefore(alternative, wrapper[0]);

    var heading = document.createElement('h2');
    heading.classList.add('products-alternative-header');
    heading.innerHTML = i18n['ALTERNATIVE_PRODUCTS_HEADING_TEXT'];

    alternative.parentNode.insertBefore(heading, alternative);

    var tabLink = document.querySelectorAll('[href="#productsAlternative"]');
    if (tabLink) {
        tabLink[0].classList.add('js-hidden');
    }
}


function getEanResource() {
    var eanVersion = denon.eanVersion || 1;
    if (typeof(Storage) !== "undefined") {
        var eanResource = localStorage.getItem('eanResource');
        var eanResourceVersion = localStorage.getItem('eanResourceVersion');
        if (!eanResource || (eanResourceVersion < eanVersion)) {
            fetch(
                'https://419667.myshoptet.com/cache/user/documents/exports/ean.csv')
                .then(response => response.text())
                .then(
                    function(data) {
                        localStorage.setItem('eanResource', data);
                        localStorage.setItem('eanResourceVersion', eanVersion);
                        processEan(data);
                    }
                );
        } else {
            processEan(eanResource);
        }
    }
}

function processEan(eanResource) {
    var product = dataLayer[0].shoptet['product'];
    var splitResource = eanResource.split(/\r\n|\n/);
    for (var i = 0; i < splitResource.length; i++) {
        var splitLine = splitResource[i].split(/;/);
        if (splitLine[0] === '"' + product.code + '"') {
            var code = document.querySelectorAll('.p-code')[0];
            if (code) {
                var ean = document.createElement('span');
                ean.classList.add('p-ean');
                ean.innerHTML = 'EAN: ' + splitLine[3].replace(/['"]+/g, '');
                code.appendChild(ean);
            }
            break;
        }
    }
}

function groupShippingItems() {
    if (shoptet.config.orderingProcess.step !== 1) {
        return false;
    }
    if (typeof denon.shippingItemsToGroup === 'undefined') {
        return false;
    }
    var shippingMethods = document.getElementById('order-shipping-methods');
    var groupedItemsHeader = document.createElement('div');
    groupedItemsHeader.classList.add('grouped-items-header');
    shippingMethods.parentNode.insertBefore(groupedItemsHeader, shippingMethods);


    var groupedItemsLabel = document.createElement('div');
    groupedItemsLabel.classList.add('grouped-items-label');
    groupedItemsLabel.classList.add('for-free');
    groupedItemsLabel.innerHTML = denon.shippingGroupedItemsLabel;

    var select = document.createElement('select');
    select.classList.add('grouped-shipping-select');
    select.classList.add('form-control');
    var option = document.createElement('option');
    option.classList.add('grouped-default-option');
    option.value = '';
    option.innerHTML = i18n.CHOOSE_A_PICKUP;
    select.appendChild(option);

    groupedItemsHeader.appendChild(select);
    groupedItemsHeader.appendChild(groupedItemsLabel);

    for (var i = 0; i < denon.shippingItemsToGroup.length; i++) {
        var itemToGroup = document.getElementById(denon.shippingItemsToGroup[i]);
        itemToGroup.classList.add('grouped-shipping-item');
        var shippingName = itemToGroup.querySelector('.shipping-billing-name');
        shippingName.innerHTML = shippingName.innerHTML.replace(denon.shippingItemLabelToGroup, '');

        var tooltipContent = itemToGroup.querySelector('[data-original-title]');
        var tooltipWrapper = document.createElement('div');
        if (tooltipContent && tooltipWrapper) {
            tooltipWrapper.classList.add('grouped-item-tooltip');
            tooltipWrapper.innerHTML = tooltipContent.getAttribute('data-original-title');
            itemToGroup.appendChild(tooltipWrapper);
        }

        option = document.createElement('option');
        option.value = denon.shippingItemsToGroup[i];
        option.innerHTML = shippingName.innerHTML.replace(denon.shippingItemLabelToGroup, '');
        select.appendChild(option);
    }


    select.addEventListener('change', function() {
        var defaultOption = this.querySelector('.grouped-default-option');
        if (defaultOption) {
            defaultOption.remove();
        }
        var shipping = document.getElementById(this.value);
        shoptet.scripts.signalNativeEvent('mousedown', shipping);
    });

    var toggleUnselected = document.querySelector('.cart-toggle-unselected-options[data-table="order-shipping-methods"]');

    toggleUnselected.addEventListener('click', function() {
        var select = document.querySelector('.grouped-shipping-select');
        select.classList.remove('js-hidden');
        var shipping = document.getElementById(select.value);
        shoptet.scripts.signalNativeEvent('mousedown', shipping);
        var label = document.querySelector('.grouped-items-label');
        label.classList.remove('js-hidden');
    });

    document.addEventListener('shoptet.checkoutShared.replacingChosenShippingAndBilling', function() {
        var select = document.querySelector('.grouped-shipping-select');
        select.classList.add('js-hidden');
        var label = document.querySelector('.grouped-items-label');
        label.classList.add('js-hidden');
        for (var i = 0; i < denon.shippingItemsToGroup.length; i++) {
            if (document.getElementById(denon.shippingItemsToGroup[i]).classList.contains('active')) {
                select.classList.remove('js-hidden');
                label.classList.remove('js-hidden');
                break;
            }
        }
    });

}

document.addEventListener('DOMContentLoaded', function() {
    generateLinks();
    handleWithMenu();
    var products = document.querySelectorAll('.products .p');
    adjustProductsList(products);
    renameButtons();
    cloneSearch();
    adjustHeader();
    adjustHeaderButtons();
    moveFlags();
    moveRelated();
    groupShippingItems();
    var lastScrollTop = 0;
    detectScroll('up');
    var body = document.getElementsByTagName('body')[0];
    if (body.classList.contains('type-detail')) {
        getEanResource();
    }
    $(window).scroll(function() {
        var st = $(this).scrollTop();
        detectScroll(lastScrollTop < st ? 'down' : 'up');
        lastScrollTop = st;
    });
});
// Handle with product list after AJAX
var events = ['ShoptetDOMPageMoreProductsLoaded', 'ShoptetDOMPageContentLoaded'];
for (i = 0; i < events.length; i++) {
    document.addEventListener(events[i], function() {
        var products = document.querySelectorAll('.products .p');
        adjustProductsList(products);
        renameButtons();
    });
}