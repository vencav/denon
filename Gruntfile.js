module.exports = function(grunt) {
    var pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({
        less: {
            css: {
                options: {
                    javascriptEnabled: true,
                    compress: false,
                },
                files: {
                    'dist/main.css': 'less/main.less'
                }
            }
        },
        watch: {
            css: {
                files: '**/*.less',
                tasks: ['less:css'],
                options: {
                    spawn: false,
                    interrupt: true,
                    livereload: 35729
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('compile', ['less:css']);

};
