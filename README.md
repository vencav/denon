# Template for Denon shop on Shoptet platform

## How to generate assets
```
cd path_to_my_folder
git clone git@bitbucket.org:vencav/denon.git
cd denon
npm install
grunt
```

Command `grunt` starts the watch mode, so when you edit any less file, the CSS output file will be compiled.
You can also use command `grunt compile` to simply compile CSS without starting the watch mode.

## How to apply styles to Shoptet e-shop

### Upload files to FTP

Upload generated files to user/documents/assets.

### Include assets via /admin/html-kody/

#### Header section

Header section must contains this script to remove Shoptet default CSS:

```
<script>
var styles = document.querySelectorAll('link[rel="stylesheet"]');
for (i = 0; i < styles.length; i++) {
    if (styles[i].getAttribute('href').indexOf('main') !== -1) {
        styles[i].parentNode.removeChild(styles[i]);
    }
}
</script>
```

Then you have to include CSS assets:

```
<link href="https://cdn.myshoptet.com/usr/419667.myshoptet.com/user/documents/assets/main.css?v=20210515093126" rel="stylesheet">
```

#### Footer section

For purposes of this particular template you have to include this piece of JS:

```
<script>
    var denon = {};
    denon.eanVersion = 1;
    denon.shippingItemLabelToGroup = 'Osobně u partnera - ';
    denon.shippingGroupedItemsLabel = '<strong>Osobně u partnera:</strong>';
    denon.shippingItemsToGroup = [
      'shipping-95',
      'shipping-104',
      'shipping-107',
      'shipping-110',
      'shipping-113',
      'shipping-116',
      'shipping-119',
      'shipping-122',
      'shipping-125',
      'shipping-128',
      'shipping-131',
      'shipping-134',
      'shipping-137',
      'shipping-140',
      'shipping-143'
    ];
    shoptet.abilities.feature.tabs_responsive = false;
</script>
```

And finally include JS scripts:

```
<script src="https://cdn.myshoptet.com/usr/419667.myshoptet.com/user/documents/assets/main.js?v=20210515093126"></script>
```

**Always use paths using `cdn.myshoptet.com` !!!**

### How to force new version of CSS/JS

Simply change the hash after `v=`.

## How to enable EAN in product detail

- create products export type called `ean`
- it should contain only `code`, `name` and `ean`
- generate it and upload it to FTP to `user/documents/exports`

### How to force new version of EANs?

Regenerate the EAN export, upload it to FTP and increase number for `denon.eanVersion`
in footer section.

### Grouping shipping items in checkout

It's configured via `shippingItemsToGroup` - it depends on id's of shipping methods set in admin.
Also `shippingItemLabelToGroup` relies on exact naming of shipping items.
In `shippingGroupedItemsLabel` you can set HTML you want to display as "label" of grouped items.

## Tips for continuous development

- use variables
- don't ever edit anything in `node_modules`
- I've used Bootstrap less port because of the Bootstrap version used in Shoptet 3G
- I've attached `bootstrap-less-port.zip` for case that port will be no longer available
